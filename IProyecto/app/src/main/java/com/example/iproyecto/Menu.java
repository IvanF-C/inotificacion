package com.example.iproyecto;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Menu extends AppCompatActivity {
    public String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        final Button btnGuardarDes=(Button) findViewById(R.id.buttonGuardarDes);
        btnGuardarDes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText etTitulo=(EditText) findViewById(R.id.editTextTitulo);
                EditText etDescripcion=(EditText) findViewById(R.id.editTextDescripcion);
                String titulo=etTitulo.getText().toString();
                String descripcion=etDescripcion.getText().toString();

                if (TextUtils.isEmpty(titulo))
                {
                    etTitulo.setError("Ingrese un Titulo al Comentario");
                    etTitulo.requestFocus();
                    return;
                }

                if (TextUtils.isEmpty(descripcion))
                {
                    etDescripcion.setError("Ingrese una Descripcion a su Comentario");
                    etDescripcion.requestFocus();
                    return;
                }


/*
                ServicioPeticion service =API.getApi(Menu.this).create(ServicioPeticion.class);
                Call<DatosMenu> datosMenuCall=service.guardarComentario();
                datosMenuCall.enqueue(new Callback<DatosMenu>() {
                    @Override
                    public void onResponse(Call<DatosMenu> call, Response<DatosMenu> response) {
                        DatosMenu menu=response.body();

                    }

                    @Override
                    public void onFailure(Call<DatosMenu> call, Throwable t) {

                    }
                });
*/
            }
        });

    }
    public void IrTodosUsuarios(View view)
    {
        Intent Inicio= new Intent(Menu.this,Detalles.class);
        startActivity(Inicio);
    }
}

