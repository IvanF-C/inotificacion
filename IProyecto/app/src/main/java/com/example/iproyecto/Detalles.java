package com.example.iproyecto;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Detalles extends AppCompatActivity {
    ArrayList<String> ListaCompletaUsuarios=new ArrayList<>();
    ArrayList<String> ListaCompletaID=new ArrayList<>();
    ArrayList<String> ListaCompletaContrasena=new ArrayList<>();

    public ListView lvUsuarios;
    public TextView tvDetallesUsuarios,tvDetallesContrasena,tvDetallesId;

    public String Detalles [];
    public String Password [];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalles);

        tvDetallesUsuarios=(TextView) findViewById(R.id.textViewDetallesUsuarios);
        tvDetallesId=(TextView) findViewById(R.id.textViewDetallesId);
        tvDetallesContrasena=(TextView) findViewById(R.id.textViewDetallesContrasena);
        lvUsuarios=(ListView) findViewById(R.id.listViewUsuarios);

        final ArrayAdapter arrayAdapter= new ArrayAdapter(this,R.layout.activity_detalles,ListaCompletaUsuarios);

        lvUsuarios.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                tvDetallesUsuarios.setText("Usuario: " + lvUsuarios.getItemAtPosition(position));
                tvDetallesId.setText("Id: " + Detalles[position]);
                tvDetallesContrasena.setText("Password: " + Password[position]);
            }
        });

        ServicioPeticion service=API.getApi(Detalles.this).create(ServicioPeticion.class);
        Call<DatosListaUsuarios> datosListaUsuariosCall=service.todosUsuarios();
        datosListaUsuariosCall.enqueue(new Callback<DatosListaUsuarios>() {
            @Override
            public void onResponse(Call<DatosListaUsuarios> call, Response<DatosListaUsuarios> response) {
                DatosListaUsuarios peticion=response.body();

                if (peticion.estado.equals("true"))
                {
                    List<ArregloUsuario> usuarios = peticion.usuarios;

                    for (ArregloUsuario mostrar: usuarios)
                    {
                        ListaCompletaUsuarios.add(mostrar.getUsername());
                    }
                    lvUsuarios.setAdapter(arrayAdapter);
                    for (ArregloUsuario mostrarId:usuarios)
                    {
                        ListaCompletaID.add(mostrarId.getId());
                    }
                    Detalles = ListaCompletaID.toArray(new String[ListaCompletaID.size()]);

                    for (ArregloUsuario mostrarContrasena:usuarios)
                    {
                        ListaCompletaContrasena.add(mostrarContrasena.getPassword());
                    }

                }
                else
                {
                    Toast.makeText(Detalles.this,"Ocurrio un Error 1",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<DatosListaUsuarios> call, Throwable t) {
                Toast.makeText(Detalles.this,"Ocurrio un Error 2",Toast.LENGTH_LONG).show();

            }
        });

    }


}
