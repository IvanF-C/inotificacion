package com.example.iproyecto;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Registro extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        Button btnGuardar=(Button)findViewById(R.id.buttonRegistro);
        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText nameUsuarios=(EditText)findViewById(R.id.editTextRegistroUsuario);
                EditText cont=(EditText)findViewById(R.id.editTextRegistroContrasena);
                EditText cont2=(EditText)findViewById(R.id.editTextRegistroRContrasena);

                String usuarioName=nameUsuarios.getText().toString();
                String contrasena=cont.getText().toString();
                String contrasena2=cont2.getText().toString();

                if (TextUtils.isEmpty(usuarioName))
                {
                    nameUsuarios.setError("Por favor, Ingrese un nombre de Usuario");
                    nameUsuarios.requestFocus();
                    return;
                }

                if (TextUtils.isEmpty(contrasena))
                {
                    cont.setError("Por favor, Ingrese su Contraseña");
                    cont.requestFocus();
                    return;
                }

                if (TextUtils.isEmpty(contrasena2))
                {
                    cont2.setError("Por favor, Ingrese Repita Contraseña");
                    cont2.requestFocus();
                    return;
                }


                if (!contrasena.equals(contrasena2))
                {
                    cont.setError("Las contraseñas no coinciden");
                    cont.requestFocus();
                    cont2.setError("Las contraseñas no coinciden");
                    cont2.requestFocus();
                    return;

                }


                ServicioPeticion service=API.getApi(Registro.this).create(ServicioPeticion.class);
                Call<DatosRegistro> datosRegistroCall=service.datosRegistro(nameUsuarios.getText().toString(),cont.getText().toString());
                datosRegistroCall.enqueue(new Callback<DatosRegistro>() {
                    @Override
                    public void onResponse(Call<DatosRegistro> call, Response<DatosRegistro> response) {
                        DatosRegistro peticion=response.body();
                        if (response.body()==null)
                        {
                            Toast.makeText(Registro.this,"Se presento un Erro",Toast.LENGTH_LONG).show();
                            return;
                        }
                        if (peticion.estado=="true")
                        {
                            startActivity(new Intent(Registro.this,MainActivity.class));
                            Toast.makeText(Registro.this,"Registro Exitoso",Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                            Toast.makeText(Registro.this,"Hubo un Error en el Registo",Toast.LENGTH_LONG).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<DatosRegistro> call, Throwable t) {
                        Toast.makeText(Registro.this,"Error de conexion",Toast.LENGTH_LONG).show();

                    }
                });
            }
        });
    }


    public void RegresarInicio(View view)
    {
        Intent Inicio= new Intent(Registro.this,MainActivity.class);
        startActivity(Inicio);
    }
}

