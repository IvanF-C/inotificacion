package com.example.iproyecto;

public class DatosMenu {
    public String usuarioId;
    public String titulo;
    public String descripcion;
    public String estado;

    public DatosMenu(String usuarioId, String titulo,String descripcion)
    {
        this.usuarioId=usuarioId;
        this.titulo=titulo;
        this.descripcion=descripcion;
    }


    public String getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(String usuarioId) {
        this.usuarioId = usuarioId;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
