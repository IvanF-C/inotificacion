package com.example.iproyecto;

import java.util.ArrayList;

public class DatosListaUsuarios {
    public String estado;
    public String detalle;
    public ArrayList<ArregloUsuario> usuarios;

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public ArrayList<ArregloUsuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(ArrayList<ArregloUsuario> usuarios) {
        this.usuarios = usuarios;
    }
}
